import { Component, OnInit } from '@angular/core';
import { StringifyOptions } from 'querystring';
import {ChatService} from "../../providers/chat.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})

export class LoginComponent implements OnInit {

  constructor(public cs:ChatService) { }
  ngOnInit () {}

 ingresar(){
    console.log('cargando login');
 
    this.cs.login();
}
} 