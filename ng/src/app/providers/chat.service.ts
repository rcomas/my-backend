import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {auth}  from 'firebase/app';
 
@Injectable()
export class ChatService {


   public usuario:any={};
  
  constructor(public afauth:AngularFireAuth) { 

    this.afauth.authState.subscribe(user=>{
      console.log( 'Estado del usuario: ', user);
        if(!user){
          return;
        }
        this.usuario.nombre=user.displayName;
        this.usuario.uid=user.uid;
    })
  }

  login() {
    this.afauth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }
  logout() {
    this.usuario={};
    this.afauth.auth.signOut();
  }
  }

