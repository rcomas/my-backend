import { BrowserModule } from '@angular/platform-browser';
import { NgModule  } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AppComponent } from './app.component';
//Componentes
import { LoginComponent } from './components/login/login.component';

//Servicios
import {ChatService} from "./providers/chat.service";
import { ChatComponent } from './components/chat/chat.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ChatComponent,
  ],
  exports: [LoginComponent
  ],
 
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
  
     ],
     
  providers: [ChatService
   ],
   
  bootstrap: [AppComponent]
})
export class AppModule { }

