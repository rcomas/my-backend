import { Component } from '@angular/core';
import { ChatService } from 'src/app/providers/chat.service';
import { environment } from 'src/environments/environment.prod';
import { VERSION } from 'src/environments/version';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'my-backend';
  version=VERSION.version;
  tag=VERSION.tag;
  hash=VERSION.hash;
  hashes: '${gitHash}';
  built: '${buildHash}';

  
  constructor(public _cs:ChatService)
  {
 
     
    
  }
}
