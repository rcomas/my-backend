package com.jummit.chatless;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ChatlessApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChatlessApplication.class, args);
	}
	
	
}
